﻿using Clockwork.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Clockwork
{
    public class Clockwork : IClockwork
    {
        /// <summary>
        /// GetCurrentTimeList
        /// </summary>
        /// <returns>CurrentTimeList</returns>
        public List<CurrentTimeQuery> GetCurrentTimeList(string ip, string timeZone)
        {
            try
            {
                var utcTime = DateTime.UtcNow;

                TimeZoneInfo customTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                DateTime customTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, customTimeZone);
                var currentTimeList = new List<CurrentTimeQuery>();
                var currentTime = new CurrentTimeQuery
                {
                    UTCTime = utcTime,
                    ClientIp = ip,
                    Time = customTime
                };
                using (var db = new ClockworkContext())
                {
                    db.CurrentTimeQueries.Add(currentTime);
                    var count = db.SaveChanges();
                    currentTimeList = db.CurrentTimeQueries.OrderByDescending(x=>x.CurrentTimeQueryId).ToList();
                }
                return currentTimeList;
            }
            catch (Exception ex)//TODO : log error message
            {
                return null;//Return null for now
            }
        }
    }
}
