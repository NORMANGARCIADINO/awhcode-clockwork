﻿using Clockwork.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Clockwork
{
    interface IClockwork
    {
       List<CurrentTimeQuery> GetCurrentTimeList(string ip, string timeZone);
    }
}
