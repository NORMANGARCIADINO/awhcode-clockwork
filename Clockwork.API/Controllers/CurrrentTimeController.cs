﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Linq;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CurrentTimeController : Controller
    {
        private API.Clockwork.IClockwork clockwork = new API.Clockwork.Clockwork();
        // GET api/currenttime/GetCurrentTimeList
        [HttpGet]
        public IActionResult GetCurrentTimeList(string timezone)
        {
            var returnVal = clockwork.GetCurrentTimeList(this.HttpContext.Connection.RemoteIpAddress.ToString(), timezone);
            return new JsonResult(returnVal);
        }
    }
}
